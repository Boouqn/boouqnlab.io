+++
title = "extensions I use"
author = ["Wiktor Cooley"]
date = 2020-02-09
lastmod = 2021-02-09T15:13:43-06:00
tags = ["technology"]
draft = false
weight = 2001
+++

sorted by importance

1.  ublock origin: block ads & other malicious stuff on the web
2.  dark reader: dark websites everywhere on the web
3.  edit with emacs
4.  grasp: book mark server
5.  wappalyzer: figure out what a website is using
