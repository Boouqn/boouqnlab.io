+++
title = "Linux Config"
author = ["Wictor Cooley"]
date = 2021-01-30
lastmod = 2021-02-10T16:37:56-06:00
tags = ["emacs"]
draft = false
+++

## introduction {#introduction}


### why literate? {#why-literate}

> The effect of this simple shift of emphasis can be so profound as to change
> one's whole approach to programming. Under the literate programming paradigm,
> the central activity of programming becomes that of conveying meaning to other
> intelligent beings rather than merely convincing the computer to behave in a
> particular way. It is the difference between performing and exposing a magic
> trick. -- [Ross Williams](http://literateprogramming.com/index.html)

-   easier to comprehend your dotfiles
-   easier to manage 1 file then multiple files & directories
-   easier to manage in git
-   see reasons by others ([knuth](https://github.com/limist/literate-programming-examples), [schwartz](https://harryrschwartz.com/2016/05/19/when-is-literate-programming-appropriate))


### why X? {#why-x}


#### what I value {#what-i-value}

-   keyboard centrism
-   text centrism
-   interoperability
-   extensibility


### references {#references}

-   [zzamboni](https://zzamboni.org/post/my-doom-emacs-configuration-with-commentary/)
-   [tecosaur](https://tecosaur.github.io/emacs-config/config.html)


## fish {#fish}

the 90s shell, it give auto-completion & other goods by default, I installed fisher
(a package manager for fish) and from there installed z (a better cd) and fzf (fuzzy
finding) which has improved greatly my terminal experience.

im setting the environmental variables manually since sometimes it doesn't pick up.

<a id="code-snippet--config.fish"></a>
```conf
set PATH /home/mutecity/bin $PATH

set PATH /home/mutecity/.local/bin $PATH
```

[the bookmark server](https://github.com/karlicoss/grasp)

```conf
alias bserver="~/code/grasp/server/grasp_server.py --path ~/writings/capture.org"
```


## email {#email}

| **pros**                                       | **cons**                                    |
|------------------------------------------------|---------------------------------------------|
| 1 unified UI                                   | takes time to set-up                        |
| suited to your needs                           | some websites force you to use their webapp |
| could be used for decades                      | security issues (if not careful)            |
| emails can last forever (even if website gone) | issues in viewing images                    |
|                                                | usually cant see css styling                |

getmail retrieves mail client

```conf
[retriever]
type = SimpleIMAPSSLRetriever
server = imap.gmail.com
username = swbvty@gmail.com
password = <SECRET>

[destination]
type = Maildir
path = ~/mail/

[options]
verbose = 2
message_log = ~/.getmail/log

# retrieve only new message
read_all = false

# do not alter messsages
delivered_to = false
received = false
```

msmtp is used to sendmail

```conf
defaults
auth on
tls on
tls_trust_file /etc/ssl/certs/ca-certificates.crt
logfile ~/.msmtp.log

account gmail
host    smtp.gmail.com
port    587
from    swbvty@gmail.com
user    swbvty
password <SECRET>
account default : gmail
```

notmuch index my mail so that it can be viewed in a client (such as notmuch-emacs)

```conf
[database]
path=/home/mutecity/mail

[user]
name=swbvty
primary_email=swbvty@gmail.com

[new]
tags=unread;inbox;
ignore=

[search]
exclude_tags=deleted;spam;

[maildir]
synchronize_flags=true
```


## emacs {#emacs}

im not sure how I should divide the configuration, by purpose, by what it is or
some other schema. But I think the best idea is to divide by what it is &
describing what I want and get from it. I don't want to fall into the trap of
thinking literate programming is a documentation system and not a paradigm but
this makes logical sense.


### visuals {#visuals}

monospace is good for code, most people prefer prose in variable pitch.

```
(setq doom-font (font-spec :family "Fira Code" :size 18)
      doom-variable-pitch-font (font-spec :family "ETBembo" :size 18))
```

Allow mixed fonts in a buffer. This is particularly useful for writing & literate programming,

```
(package! mixed-pitch)
```

enable it

```
(add-hook! 'org-mode-hook #'mixed-pitch-mode)
(setq mixed-pitch-variable-pitch-cursor nil)
```

default theme I change this a lot.

```
(setq doom-theme 'doom-one-light)
```

very useful to have the time & battery percentage in the status bar, especally for EXWM

```
(display-battery-mode 1)
(display-time-mode 1)
```

I prefer folded so I can see the whole hierarchy

```
(setq org-startup-folded t)
```


### notetaking {#notetaking}

Deft brings a wonderful search interface for your notes. it's one of my favorite packages.

```
(setq deft-extensions '("org" "md" "txt"))
(setq deft-directory <<notes>>)
(setq deft-use-filename-as-title t)
```

org-mode is a outliner and that makes short noters overall IMO redudant.

<a id="code-snippet--notes"></a>
```
"~/O/zettels/"
```


### feed reading {#feed-reading}

Elfeed is a terrific RSS reader that is very customizable. with the addition of
elfeed-org I can annotate my feed listing and neatly sort them in a hierarchy.

install it

```
(package! elfeed)
(package! elfeed-org)
```

enable it

```
(elfeed-org)

(setq rmh-elfeed-org-files (list "~/elfeed.org"))
```


### dictionary {#dictionary}

this is experimental and im not sure works, sdcv-mode & lexic (new) are the two
common way to search up definitions locally.  Both rely on the sdcv command line
program.

I am using the [webster 1913 dictionary](https://jsomers.net/blog/dictionary), very well-written entries. As well as the [jargon file](http://www.catb.org/~esr/jargon/) since it's fun.

```
(add-to-list 'load-path "~/.doom.d/lisp")
(require 'sdcv-mode)

(map! :leader
      :desc "search definition"
      "a j" #'sdcv-search)
```

disabling spellchecking for now, getting a lot of errors on linux.


### window manager {#window-manager}

I usually use xfce but I do want to experiment with this one. I don't use a
tiling WM anymore, but this has the advantage of integrating with emacs. Ive
heard some problems in the use of taking zoom calls or playing games (maybe due
to it's emacs single threaded nature) but these are things I don't do on my
laptop.

this is still very much experimental

I want xahlee style keybindings

| function        | keybinding |
|-----------------|------------|
| fullscreen      | f1         |
| launcher        | f2         |
| switcher        | f3         |
| previous window | f4         |
| kill window     | f5         |
| feedreader      | f9         |
| browser         | f10        |
| shell           | f11        |

```
(package! exwm)
```

```
(use-package exwm
  :config
    ;; necessary to configure exwm manually
    (require 'exwm-config)

    ;; fringe size, most people prefer 1
    (fringe-mode 3)

    ;; emacs as a daemon, use "emacsclient <filename>" to seamlessly edit files from the terminal directly in the exwm instance
    ;; (server-start)

    (setq exwm-input-prefix-keys
    '(?\C-x
      ?\C-u
      ?\ \
      ?\M-`
      ?\M-&
      ?\M-:
      ?\C-\M-j  ;; Buffer list
      ?\C-\ ))  ;; Ctrl+Space


    ;; this fixes issues with ido mode, if you use helm, get rid of it
    (exwm-config-ido)

    ;; this is a way to declare truly global/always working keybindings
    (exwm-input-set-key (kbd "<f9>") 'elfeed)
    (exwm-input-set-key (kbd "<f3>") 'ibuffer)
    (exwm-input-set-key (kbd "<f1>") 'delete-other-windows)
    (exwm-input-set-key (kbd "<f4>") #'er-switch-to-previous-buffer)
    ;; (setq exwm-input-global-keys `( ([<f3>] . ibuffer) ))

    ;; the simplest launcher
    (exwm-input-set-key (kbd "<f2>")
                        (lambda (command)
                          (interactive (list (read-shell-command "{-> ")))
                          (start-process-shell-command command nil command)))

    ;; this little bit will make sure that XF86 keys work in exwm buffers as well
    (exwm-input-set-key (kbd "<XF86AudioLowerVolume>")
                    (lambda () (interactive) (shell-command "amixer set Master 2%-")))
    (exwm-input-set-key (kbd "<XF86AudioRaiseVolume>")
                    (lambda () (interactive) (shell-command "amixer set Master 2%+")))
    (exwm-input-set-key (kbd "<XF86AudioMute>")
                    (lambda () (interactive) (shell-command "amixer set Master 1+ toggle")))
    ;; this little bit will make sure that XF86 keys work in exwm buffers as well
    ;; this just enables exwm, it started automatically once everything is ready
    (exwm-enable)
  :hook
    ((exwm-update-title . farl-exwm/name-buffer-after-window-title)))
```


## change log {#change-log}

will probably just makes this git commits, but I think more detailed prose
might better. Git log is here


### 2021-02-09 Tue {#2021-02-09-tue}

redesigned my website, much better


### 2022-02-01 Tue {#2022-02-01-tue}

got my hugo website running


### 2021-01-31 Sun {#2021-01-31-sun}

did some research and got the dictionary to work. Actually was interesting to read about.


### 2021-01-30 Sat {#2021-01-30-sat}

started this literate programming project


## todo log {#todo-log}

-   [X] expand to include you whole doom emacs config <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-01-30 Sat&gt; </span></span> <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-02-01 Mon&gt;</span></span>
-   [X] add email config <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-01-30 Sat&gt;</span></span>
-   [X] set up dictionary to work with emacs <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-01-30 Sat&gt; </span></span> <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-01-31 Sun&gt;</span></span>
-   [X] get mixed fonts to work <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-01-30 Sat&gt; </span></span> <span class="timestamp-wrapper"><span class="timestamp">&lt;2022-02-01 Tue&gt;</span></span>
-   [X] upload to gitlab as hugo project <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-01-31 Sun&gt; </span></span> <span class="timestamp-wrapper"><span class="timestamp">&lt;2022-02-01 Tue&gt;</span></span>
-   [X] make this a git repository <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-02-01 Mon&gt;</span></span>
-   [X] fix aspell errors, everything is considered incorrect for some reason <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-02-01 Mon&gt;</span></span>
-   [ ] check out emacs everywhere <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-02-02 Tue&gt;</span></span>
-   [ ] get spellchecking working and be able to add words <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-02-09 Tue&gt;</span></span>
-   [X] get function keys to be in global mode (line mode)? <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-02-05 Fri&gt; </span></span> <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-02-09 Tue&gt;</span></span>
-   [X] windows named correctly <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-02-06 Sat&gt; </span></span> <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-02-09 Tue&gt;</span></span>
-   hugo website
    -   [X] get code blocks to be unified in theme <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-02-01 Mon&gt; </span></span> <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-02-09 Tue&gt;</span></span>
    -   [X] populate sidebar with correct links <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-02-01 Mon&gt;</span></span>
    -   [X] have a blog index <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-02-01 Mon&gt;</span></span>
