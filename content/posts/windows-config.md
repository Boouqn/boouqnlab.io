+++
title = "windows config"
author = ["Wiktor Cooley"]
date = 2021-01-30
lastmod = 2021-02-10T16:41:22-06:00
tags = ["emacs"]
draft = false
+++

## introduction {#introduction}


### why literate? {#why-literate}

> The effect of this simple shift of emphasis can be so profound as to change
> one's whole approach to programming. Under the literate programming paradigm,
> the central activity of programming becomes that of conveying meaning to other
> intelligent beings rather than merely convincing the computer to behave in a
> particular way. It is the difference between performing and exposing a magic
> trick. -- [Ross Williams](http://literateprogramming.com/index.html)

-   easier to comprehend your dotfiles
-   easier to manage 1 file then multiple files & directories
-   easier to manage in git
-   see reasons by others ([knuth](https://github.com/limist/literate-programming-examples), [schwartz](https://harryrschwartz.com/2016/05/19/when-is-literate-programming-appropriate))


### why X? {#why-x}


#### what I value {#what-i-value}

-   keyboard centrism
-   text centrism
-   interoperability
-   extendability


### references {#references}

-   [zzamboni](https://zzamboni.org/post/my-doom-emacs-configuration-with-commentary/)
-   [tecosaur](https://tecosaur.github.io/emacs-config/config.html)
-   [uncle dave](https://github.com/daedreth/UncleDavesEmacs/blob/master/config.org)


## fish {#fish}

the 90s shell, it give auto-completion & other goods by default, I installed fisher
(a package manager for fish) and from there installed z (a better cd) and fzf (fuzzy
finding) which has improved greatly my terminal experience.

im setting the environmental variables manually since sometimes it doesn't pick up.

<a id="code-snippet--config.fish"></a>
```conf
set PATH /home/mutecity/bin $PATH

set PATH /home/mutecity/.local/bin $PATH
```

[the bookmark server](https://github.com/karlicoss/grasp)

```conf
alias bserver="~/code/grasp/server/grasp_server.py --path ~/writings/capture.org"
```


## email {#email}

| **pros**                                       | **cons**                                    |
|------------------------------------------------|---------------------------------------------|
| 1 unified UI                                   | takes time to set-up                        |
| suited to your needs                           | some websites force you to use their webapp |
| could be used for decades                      | security issues (if not careful)            |
| emails can last forever (even if website gone) | issues in viewing images                    |
| can't see css styling                          | usually cant see css styling                |

getmail retrieves mail client

```conf
[retriever]
type = SimpleIMAPSSLRetriever
server = imap.gmail.com
username = swbvty@gmail.com
password = <SECRET>

[destination]
type = Maildir
path = ~/mail/

[options]
verbose = 2
message_log = ~/.getmail/log

# retrieve only new message
read_all = false

# do not alter messsages
delivered_to = false
received = false
```

msmtp is used to sendmail

```conf
defaults
auth on
tls on
tls_trust_file /etc/ssl/certs/ca-certificates.crt
logfile ~/.msmtp.log

account gmail
host    smtp.gmail.com
port    587
from    swbvty@gmail.com
user    swbvty
password <SECRET>
account default : gmail
```

notmuch index my mail so that it can be viewed in a client (such as notmuch-emacs)

```conf
[database]
path=/home/mutecity/mail

[user]
name=swbvty
primary_email=swbvty@gmail.com

[new]
tags=unread;inbox;
ignore=

[search]
exclude_tags=deleted;spam;

[maildir]
synchronize_flags=true
```


## emacs {#emacs}

im not sure how I should divide the configuration, by purpose, by what it is or
some other schema. But I think the best idea is to divide by what it is &
describing what I want / get from it. I don't want to fall into the trap of
thinking literate programming is a documentation system and not a paradigm but
this makes logical sense.


### visuals {#visuals}

monospace is good for code, most people prefer prose in variable pitch.

```
(setq doom-font (font-spec :family "Fira Code" :size 18)
      doom-variable-pitch-font (font-spec :family "ETBembo" :size 18))


```

Allow mixed fonts in a buffer. This is particularly useful for writing & literate programming,

```
(package! mixed-pitch)
```

enable it

```
(add-hook! 'org-mode-hook #'mixed-pitch-mode)
(setq mixed-pitch-variable-pitch-cursor nil)
```

default theme I change this a lot.

```
(setq doom-theme 'doom-one-light)
```

better wrap behaviour

```
(package! visual-fill-column)
```


### notetaking {#notetaking}

Deft brings a wonderful search interface for your notes. it's one of my favorite packages.

```
(setq deft-extensions '("org" "md" "txt"))
(setq deft-directory <<notes>>)
(setq deft-use-filename-as-title t)
```

right now I store them in my archive / semi-organized folder, with other
documents. Note it is not a proper zettelkasten since they aren't short notes.
org-mode is a outliner and that makes short noters IMO redudant.

<a id="code-snippet--notes"></a>
```
"/mnt/c/Users/ffffconde/Desktop/organized-future/Documents/zettels/"
```

also using Org-roam, a zettelkasten system & note graphing tool.

```
(package! org-roam)
```

Im not using for much, but for now using it to beat minecraft.

```
(setq org-roam-directory "/mnt/c/Users/ffffconde/Desktop/organized-future/Documents/minecraft")
```

keybindings for now, im not sure how much I am a fan of it.

```
(after! org-roam
        (map! :leader
            :prefix "n"
            :desc "org-roam" "l" #'org-roam
            :desc "org-roam-insert" "i" #'org-roam-insert
            :desc "org-roam-switch-to-buffer" "b" #'org-roam-switch-to-buffer
            :desc "org-roam-find-file" "f" #'org-roam-find-file
            :desc "org-roam-show-graph" "g" #'org-roam-show-graph
            :desc "org-roam-insert" "i" #'org-roam-insert
            :desc "org-roam-capture" "c" #'org-roam-capture))
```


### feed reading {#feed-reading}

Elfeed is a terrific RSS reader that is very customizable. with the addition of
elfeed-org I can annotate my feed listing and neatly sort them in a hierarchy.

install it

```
(package! elfeed)
(package! elfeed-org)
```

enable it

```
(elfeed-org)

(setq rmh-elfeed-org-files (list "~/elfeed.org"))

(global-set-key (kbd "C-x w") 'elfeed)
```


### dictionary {#dictionary}

this is experimental and im not sure works, sdcv-mode & lexic (new) are the two
common way to search up definitions locally.  Both rely on the sdcv command line
program.

I am using the [webster 1913 dictionary](https://jsomers.net/blog/dictionary), very well-written entries. As well as the [jargon file](http://www.catb.org/~esr/jargon/) since it's fun.

```
(add-to-list 'load-path "~/.doom.d/lisp")
(require 'sdcv-mode)

(map! :leader
      :desc "search definition"
      "a j" #'sdcv-search)
```

enable spellchecking, haven't tried this yet

```
(dolist (hook '(text-mode-hook))
    (add-hook hook (lambda () (flyspell-mode 1))))
(dolist (hook '(change-log-mode-hook log-edit-mode-hook))
    (add-hook hook (lambda () (flyspell-mode -1))))
```


### edit textfields with emacs {#edit-textfields-with-emacs}

assuming you have edit-server.el in that location. This allows me to write on the internet with emacs.

```
(add-to-list 'load-path "~/.doom.d/lisp")
(require 'edit-server)
(edit-server-start)
```

Most markdown renders seem to make the first three headings levels larger than
normal text, the first two much so. Then the fourth level tends to be the same
as body text, while the fifth and sixth are (increasingly) smaller, with the
sixth greyed out. Since the sixth level is so small, I’ll turn up the boldness a
notch.

```
(add-hook! (gfm-mode markdown-mode) #'visual-line-mode #'turn-off-auto-fill)

(custom-set-faces!
  '(markdown-header-face-1 :height 1.25 :weight extra-bold :inherit markdown-header-face)
  '(markdown-header-face-2 :height 1.15 :weight bold       :inherit markdown-header-face)
  '(markdown-header-face-3 :height 1.08 :weight bold       :inherit markdown-header-face)
  '(markdown-header-face-4 :height 1.00 :weight bold       :inherit markdown-header-face)
  '(markdown-header-face-5 :height 0.90 :weight bold       :inherit markdown-header-face)
  '(markdown-header-face-6 :height 0.75 :weight extra-bold :inherit markdown-header-face))
```


## change log {#change-log}

will probably just makes this git commits, but I think more detailed prose
might better. Git log is here


### 2021-01-31 Sun {#2021-01-31-sun}

did some research and got the dictionary to work. Actually was interesting to read about.


### 2021-01-30 Sat {#2021-01-30-sat}

started this literate programming project


## todo log {#todo-log}

-   [X] expand to include you whole doom emacs config <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-01-30 Sat&gt; </span></span> <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-02-01 Mon&gt;</span></span>
-   [X] add email config <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-01-30 Sat&gt;</span></span>
-   [X] set up dictionary to work with emacs <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-01-30 Sat&gt; </span></span> <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-01-31 Sun&gt;</span></span>
-   [X] get mixed fonts to work <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-01-30 Sat&gt;</span></span>
-   [ ] get editing-server.el to run in mark-down mode <span class="timestamp-wrapper"><span class="timestamp">&lt;2022-01-31 Mon&gt;</span></span>
-   [ ] upload to gitlab as hugo project <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-01-31 Sun&gt;</span></span>
-   [ ] make this a git repository <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-02-01 Mon&gt;</span></span>
-   hugo website
    -   [ ] get code blocks to be unified in theme <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-02-01 Mon&gt;</span></span>
    -   [X] populate sidebar with correct links <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-02-01 Mon&gt;</span></span>
    -   [X] have a blog index <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-02-01 Mon&gt;</span></span>
