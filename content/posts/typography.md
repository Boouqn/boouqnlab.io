+++
title = "good web design patterns"
author = ["Wiktor Cooley"]
date = 2020-02-09
lastmod = 2021-02-09T15:13:39-06:00
tags = ["design"]
draft = false
weight = 2001
+++

Note I'm not a web designer but from what I've read of the topic, I will say
that 80-90% will agree with my assessment. I personally really enjoyed the book
"the pattern language", which is a book about patterns in architecture and urban
design, I think idea overall fits typography and web design. Starting from a
logical order of building.

1.  font:
    1.  kerning
    2.  ligatures
    3.  complex aesthetics factors (how you feel about it etc)
2.  body text:
    1.  serif font
    2.  font size: 11-16 px (maybe a bit more)
    3.  line length:  60-90 chars per line on desktop, 30-40 mobile, 45-75 in general
    4.  line height (leading): 120-145% of the pt size, 30% character height
    5.  left-aligned or justified text (plus good hyphenation)
    6.  tables: minimal possible lines
    7.  drop cap
3.  heading (note a lot of variation here):
    1.  some sort of demarcation from body text
        1.  modular scale
        2.  slightly larger / smaller then body text, more space above & below (.5 to 1 pt)
        3.  30% smaller each time from base heading
        4.  heavier weight then body text
    2.  san-serif
    3.  small caps? (this is debated)
4.  colors
    1.  either black, white or yellow body text
    2.  a bit less contrast
    3.  have contrast and also don't forget about typographic color
5.  taxonomy
    1.  tags > categories
    2.  sidenotes > footnotes
    3.  numbered table of contents > alpha-numeric (e.g 1.2 1.2.3 not 1.a.b or a.b.c)
    4.  table of content has all headings visible
    5.  three level of headings
6.  code
    1.  monospace font
7.  accessibility
    1.  contrast
    2.  alt-text for images
    3.  keyboard navigaton (follow logical reading order)
    4.  no flashing content, no moving content
    5.  video or audio alternatves for blind or deaf people
    6.  page titles brief & descriptive enough
    7.  links 3:1 contrast ratio from the non-link text surrounding it
    8.  semantic markup
    9.  light or dark mode?
8.  layout
    1.  is accessible
    2.  responsive
9.  preservability:
    1.  static website / vanilla html and css
    2.  don't minimize html
    3.  one page > many pages
    4.  no hotlinking
    5.  stick with 13 websafe fonts or non-hotlinked google fonts
    6.  have font fallback option
    7.  compress images
    8.  use monitoring services to remove broken URLS

you can probably think of things that violate these rules and are good design.
usability.yale.edu and idlewords.com have serif as header and san-serif as body.
They are pretty nice but these rules do generate a good result. Violate with
caution.

some useful links:

-   where to get nice fonts:
    -   [google fonts](https://fonts.google.com)
    -   [fontsquirrell](https://www.fontsquirrel.com/)
-   good monospace fonts:
    -   <https://devfonts.gafi.dev/>
    -   <https://coding-fonts.css-tricks.com/>
-   minimization:
    -   [svg](https://victorzhou.com/blog/minify-svgs/)
    -   [reduce size of images](https://evilmartians.com/chronicles/images-done-right-web-graphics-good-to-the-last-byte-optimization-techniques)
    -   [dither image](https://ditherit.com/)
